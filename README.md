# MASTER BRANCH will no longer build due to SDRplay removing resources from their webpage (DEPRECATED) MOVED [HERE](https://github.com/chimbosonic/dump1090-container)

`api3-docker` branch contains the official source code for dump1090 and a dockerfile that allows me to build a feature complete container.
I won't replace master as api3-docker is a fork. Also important to note https://hub.docker.com/r/chimbosonic/dump1090 is now built by `api3-docker` branch

 
# dump1090 Container
[![pipeline status](https://gitlab.com/chimbosonic/dump1090-container/badges/master/pipeline.svg)](https://gitlab.com/chimbosonic/dump1090-container/-/commits/master)

A simple container that runs https://github.com/antirez/dump1090 with SDRplay patch

Source code: https://gitlab.com/chimbosonic/dump1090-container

Docker repo: https://hub.docker.com/r/chimbosonic/dump1090

To run it you will need to share the usb device to the container:
```
docker run -it --rm --device ${USB_DEVICE_FILE} -p 8080:8080 dump1090 --dev-sdrplay --interactive --net --oversample

```

`${USB_DEVICE_FILE}` can be determined by using `lsusb` however for simplicity you can run if you have a SDRplay rspa1:

```
./scripts/get_usb_device.sh
```

All credit goes to the maintainers of dump1090 and https://www.sdrplay.com/
