FROM ubuntu:latest
LABEL maintainer="alexis.lowe@protonmail.com"
LABEL org.label-schema.schema-version="1.0"
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="chimbosonic/dump1090"
LABEL org.label-schema.description="dump1090 container"
LABEL org.label-schema.vcs-url="https://gitlab.com/chimbosonic/dump1090-container"
LABEL org.label-schema.vcs-ref=$VCS_REF

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8


COPY scripts/install.sh /opt/install.sh
RUN chmod +x /opt/install.sh && /opt/install.sh && rm -rf /opt/install.sh

ENTRYPOINT ["/opt/dump1090/dump1090"]
