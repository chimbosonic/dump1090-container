#!/bin/bash
set -e
RSP_API_URL="https://www.sdrplay.com/software/SDRplay_RSP_API-Linux-2.13.1.run"
DUMP1090_URL="https://www.sdrplay.com/software/dump1090_1.3.1.linux.tar.gz"

function install_deps() {
	apt-get update
	apt-get install -y wget ca-certificates libpsl5 openssl libssl1.1 --no-install-recommends
	apt-get install -y librtlsdr0 libusb-1.0-0 rtl-sdr --no-install-recommends
}

function install_dump1090() {
	wget ${DUMP1090_URL} -O /tmp/dump1090.tar.gz
	mkdir -p /opt/dump1090
	tar -xvf /tmp/dump1090.tar.gz --strip-components=2 -C /opt/dump1090
}

function install_rsp_api() {
	wget ${RSP_API_URL} -O /tmp/rsp_api.run
	chmod +x /tmp/rsp_api.run
	pushd /tmp
	/tmp/rsp_api.run --noexec --tar -xvf
	cp ./x86_64/libmirsdrapi-rsp.so.2.13 /usr/local/lib/.
	cp ./mirsdrapi-rsp.h /usr/local/include/.
	popd
	chmod 644 /usr/local/lib/libmirsdrapi-rsp.so.2.13 /usr/local/include/mirsdrapi-rsp.h	
	ldconfig
}

function cleanup() {
	apt-get purge -y wget ca-certificates libpsl5 openssl libssl1.1
	apt-get autoremove
	apt-get clean all
	rm -rf /var/lib/apt/lists/*
	rm -rf /tmp/**
}

install_deps
install_dump1090
install_rsp_api
cleanup
