USB_DEVICE_FILE="$(shell ./scripts/get_usb_device.sh)"

run:
	docker run -it --rm --device $(USB_DEVICE_FILE) -p 8080:8080 dump1090 --dev-sdrplay --interactive --net --oversample

build:
	docker build --build-arg BUILD_DATE=$(shell date -u +'%Y-%m-%dT%H:%M:%SZ') --build-arg VCS_REF=$(shell git rev-parse --short HEAD) -t dump1090 .
